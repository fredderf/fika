angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope, Friends, Activities) {
      $scope.friends = Friends.all();
      $scope.createActivity = function(activity) {
        Activities.create(activity);
      };
})

.controller('FriendsCtrl', function($scope, Friends) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.friends = Friends.all();
  $scope.remove = function(friend) {
    Friends.remove(friend);
  };
})

.controller('FriendDetailCtrl', function($scope, $stateParams, Friends, Activities) {
  $scope.friend = Friends.get($stateParams.friendId);
  $scope.friend.activities = Activities.get($stateParams.friendId);
})

.controller('ActivitiesCtrl', function($scope, Activities) {
  $scope.activities = Activities.all();
  $scope.removeActivity = function(activity) {
    Activities.remove(activity);
  };
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});